#!/usr/bin/env python

def fizzAndOrBuzz(input):
	"""Return FizzBuzz problem output of given input as string"""
	# Python is such a weird language.
	output = str(input) + ":\t"

	if (input % 15) == 0:
		output += "FizzBuzz"
	elif (input % 3) == 0:
		output += "Fizz"
	elif (input % 5) == 0:
		output += "Buzz"

	return output

for x in range (0, 75):
	# This is why we can't have nice things.
	print fizzAndOrBuzz(x + 1)
